var BINvalidator = (function () {
    function BINvalidator(options) {
        if (options.input === undefined || options.callback === undefined) return;
        this.url = options.url;
        this.input = options.input;
        this.callback = options.callback;
        this.init();
    }

    BINvalidator.prototype = {
        init: function () {
            this.setListener();
        },
        callAPI: function (bin) {
            var self = this;
            var binSearchRequest = null;
            if (binSearchRequest) {
                console.log('Search request is processing ...');
                binSearchRequest.abort();
            }

            binSearchRequest = $.ajax({
                url: this.url.indexOf('https://lookup.binlist.net/') > -1 ? this.url + bin : this.url,
                type: "POST",
                dataType: "json",
                data: bin,
                success: function (res) {
                    self.callback(res);
                },
                error: function (err) {
                    console.log(err);
                }
            });
        },
        setListener: function () {
            var self = this;
            self.input.keyup(function (e) {
                self.handleInput(e);
            })
        },
        handleInput: function (event) {
            var self = this;
            let userInput = $(event.target).val(),
                inputLength = userInput.length,
                trigger = 6;

            if (inputLength === trigger) {
                var bin = userInput,
                    isValidBIN = self.isNumber(bin);

                if (isValidBIN === true) {
                    self.callAPI(bin);
                } else {
                    console.log('BIN is not integer.');
                    return false;
                }
            }
        },
        isNumber: function (bin) {
            var isMatch = /^[0-9]*$/.test(parseInt(bin));
            return $.isNumeric(parseInt(bin)) && isMatch ? true : false;
        }
    }

    return BINvalidator;
})();

// ## Initialize module
var options = {
    url: 'https://lookup.binlist.net/',
    input: $('.input'),
    callback: function (res) {
        console.log(res);
    }
}
var validateBin = new BINvalidator(options);